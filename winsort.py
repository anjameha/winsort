#!/usr/bin/env python

import re
import sys


def windows_sort(l): # Sorts like Windows natural file sort in XP
    convert = lambda text: int(text) if text.isdigit() else text.lower().replace('_', '~')
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(l, key = alphanum_key)


# use stdin if it's full
if not sys.stdin.isatty():
    input_stream = sys.stdin

# otherwise, read the given filename
else:
    try:
        input_filename = sys.argv[1]
    except IndexError:
        message = 'need filename as first argument if stdin is not full'
        raise IndexError(message)
    else:
        input_stream = open(input_filename, 'ru')

print(''.join([str(item) for item in windows_sort(input_stream)]))
sys.stdout.flush()
